/* eslint-disable camelcase, no-def */
const PageController = require('../PageController')

describe('Checking Page  entity works fine.', () => {
  it('Should return the page title of the ticket using findById()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const id = '818413576'
    const pageController = new PageController()

    //  2. ACT
    const page = await pageController.findById(id)
    if (!page) {
      done.fail(`Page ${id} does not exist`)
      return
    }

    //  3. ASSERT
    expect(page.title).toBe('For testing purposes')
    done()
  })

  it('Should return true when updating the page body', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const id = '818413576'
    const pageController = new PageController()
    const page = await pageController.findById(id)
    if (!page) {
      done.fail(`Page ${id} does not exist`)
      return
    }

    const currentDate = new Date()
    const localDateString = currentDate.toLocaleDateString({
      day: 'numeric',
      month: 'short',
      year: 'numeric'
    })

    const localTimeString = currentDate.toLocaleTimeString({
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    })

    const dateString = `${localDateString} - ${localTimeString}`

    //  2. ACT
    page.body = `New body with date time ${dateString}`
    const result = await page.updateBody()

    //  3. ASSERT
    expect(result).not.toBe(false)
    done()
  })
})
