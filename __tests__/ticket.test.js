/* eslint-disable camelcase, no-def */
const TicketController = require('../TicketController')
const AtlassianClient = require('../AtlassianClient')

const atlassianClient = new AtlassianClient()

describe('Checking Ticket entity works fine.', () => {
  it('Should return the assignee of the ticket using findByKey()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const key = 'PAN-23076'
    const ticketController = new TicketController()

    //  2. ACT
    const ticket = await ticketController.findByKey(key)
    if (!ticket) {
      done.fail(`Ticket ${key} does not exist`)
      return
    }

    //  3. ASSERT
    expect(ticket.fields.assignee.name).toBe('manuel.nazar')
    done()
  })

  it('Should return not false after creating and deleting a task ticket: uses create and erase', async done => {
    // 1. ARRANGE
    const summary = `Task Test_${atlassianClient.dateFormat(new Date(), '%Y-%m-%d %H:%M:%S', true)}`
    const project = 'PAN'
    const component = 'iOS'
    const squad = 'Others'

    const ticketController = new TicketController()

    // 2. ACT CREATE
    const output = await ticketController.create(summary, project, component, squad)
    const data = JSON.parse(output)

    // 3. ASSERT CREATE
    if (!data) {
      done.fail('Could not create a JIRA Ticket')
      return
    }

    // 1. ARRANGE ERASE
    const key = data.key
    const ticket = await ticketController.findByKey(key)
    if (!ticket) {
      done.fail(`Ticket recently created ${key} does not exist`)
      return
    }

    // 2. ACT ERASE
    const new_output = await ticket.erase()
    // 3. ASSERT ERASE

    expect(new_output).not.toBe(false)

    done()
  }, 20000)

  it('Should return the number of tickets using atlassianClient.getJiraJQL()', async () => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const jql = atlassianClient.buildJQLQueryWithIssues(['PAN-23512', 'PAN-22158'])

    //  2. ACT
    const data = await atlassianClient.getJiraJQL(jql)

    //  3. ASSERT
    expect(data.total).toBe(2)
  })

  it('Should return the number of subtasks of a ticket using getSubtasks()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const key = 'PAN-23076'
    const ticketController = new TicketController()

    const ticket = await ticketController.findByKey(key)
    if (!ticket) {
      done.fail(`Ticket ${key} does not exist`)
      return
    }

    //  2. ACT
    const issues = await ticket.getSubtasks()

    //  3. ASSERT
    expect(issues.length).toBeGreaterThan(0)
    done()
  }, 10000)

  it('Should return true when setting the fix version of a ticket. Using setVersion()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const key = 'PAN-22158'
    const version = 'iOS 11.4.0'
    const ticketController = new TicketController()

    const ticket = await ticketController.findByKey(key)
    if (!ticket) {
      done.fail(`Ticket ${key} does not exist`)
      return
    }

    //  2. ACT
    const result = await ticket.setFixVersion(version)

    //  3. ASSERT
    expect(result).toBe(true)
    done()
  })

  it('Should return true when you transition to a Status. Using transitionAllTheWayToStatus()',
    async done => {
      //  1. ARRANGE
      // An environmental variable must exist: ATLASSIAN_TOKEN
      const key = 'PAN-25102'
      const destination_status = 'Done'
      const resolution = false // not needed for bugs
      const ticketController = new TicketController()

      const ticket = await ticketController.findByKey(key)
      if (!ticket) {
        done.fail(`Ticket ${key} does not exist`)
        return
      }

      //  2. ACT
      const result = await ticket.transitionAllTheWayToStatus(destination_status, resolution)

      //  3. ASSERT
      expect(result.success).toBe(true)
      done()
    }, 20000)

  it('Should return true when transition the previous Ticket to a In Analysis. Using setStatus()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const key = 'PAN-25102'
    const transition_id = 681 // In panamera project represents Analysis
    const ticketController = new TicketController()

    const ticket = await ticketController.findByKey(key)
    if (!ticket) {
      done.fail(`Ticket ${key} does not exist`)
      return
    }

    //  2. ACT
    const result = await ticket.setStatus(transition_id)

    //  3. ASSERT
    expect(result).toBe(true)
    done()
  })
})
