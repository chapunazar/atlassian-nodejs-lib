/* eslint-disable camelcase, no-def */
const VersionController = require('../VersionController')
const Version = require('../Version')
const AtlassianClient = require('../AtlassianClient')
const atlassianClient = new AtlassianClient()

describe('Checking version entity works fine.', () => {
  it('Should return the name of the version. Using findById()', async () => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const id = '30648'
    const versionController = new VersionController()

    //  2. ACT
    const version = await versionController.findById(id)

    //  3. ASSERT
    expect(version.name).toBe('iOS 11.4.0')
  })

  it('Should return the name of the version. Using findByName()', async () => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const name = 'iOS 11.4.0'
    const versionController = new VersionController()

    //  2. ACT
    const version = await versionController.findByName(name)

    //  3. ASSERT
    expect(version.id).toBe('30648')
  })

  it('Should return the number of tickets belonging to that version. Using getTickets()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const id = '30648'
    const versionController = new VersionController()
    const version = await versionController.findById(id)
    if (!version) {
      done.fail(`version ID ${id} does not exist`)
      return
    }
    //  2. ACT
    const issues = await version.getTickets()

    //  3. ASSERT
    expect(issues.length).toBe(41)
    done()
  }, 20000)

  it('Should return the Release Notes, if it returns not empry string is enough. Using getReleaseNotes', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const id = '30648'
    const versionController = new VersionController()
    const version = await versionController.findById(id)
    if (!version) {
      done.fail(`version ID ${id} does not exist`)
      return
    }
    //  2. ACT
    const rn = await version.getReleaseNotes('md')

    //  3. ASSERT
    expect(rn).not.toBe('')
    done()
  }, 20000)

  it('Create a version, edit it, release it and erase it. Using create(),save(),release() and erase()', async done => {
    //  1. ARRANGE
    // An environmental variable must exist: ATLASSIAN_TOKEN
    const versionController = new VersionController()
    const name = `Test_${atlassianClient.dateFormat(new Date(), '%Y-%m-%d', true)}`
    let version = new Version(name, 'A description')

    //  2. ACT CREATE
    let output = await version.create()

    //  3. ASSERT CREATE
    if (!output) {
      done.fail('Could not create a JIRA Version')
      return
    }

    //  2. ACT EDIT
    // Find the ID of the created version and reinitialize it
    const data = JSON.parse(output)
    version = await versionController.findById(data.id)

    version.description = 'A new description'
    version.name = `New Test_${atlassianClient.dateFormat(new Date(), '%Y-%m-%d.%H:%M:%S', true)}`
    output = await version.save()
    //  3. ASSERT EDIT
    if (!output) {
      done.fail('Could not edit a JIRA Version')
      return
    }

    //  2. ACT RELEASE
    output = await version.release()
    //  3. ASSERT RELEASE
    if (!output) {
      done.fail('Could not release a JIRA Version')
      return
    }

    //  2. ACT ERASE
    output = await version.erase()
    //  3. ASSERT ERASE
    expect(output).not.toBe(false)
    done()

  }, 20000)

})
