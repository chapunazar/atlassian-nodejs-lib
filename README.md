# Atlassian products Library for NodeJs

How to use the library in your code
-------------------------------------------------
Atlassian Library for NodeJS is made available as a script module to be called from your code.

This library uses a token that must be set in an environmental variable called ATLASSIAN_ACCESS_TOKEN.

The format of the token is as follows: "jira-user-email:auth-token-in-jira". That string needs to be encoded in Base 64.

Fianlly run in console $ export ATLASSIAN_ACCESS_TOKEN=foobar

Manipulating Ticket - Example Usage
-----------------------------------
```javascript
const TicketController = require('atlassian-nodejs-lib/TicketController')
const AtlassianClient = require('atlassian-nodejs-lib/AtlassianClient')

const atlassianClient = new AtlassianClient()

async function show(key){
  const ticketController = new TicketController()
  let ticket = await ticketController.findByKey(key)
  if (ticket){
    console.log(
      `Result from Test ${ticket.fields.assignee.name}`
      )
  }else{
    console.log(
      `Ticket does not exist`
    )
  }
}

async function createAndErase(){
  const ticketController = new TicketController()
  const summary = `Task Test_${atlassianClient.dateFormat(new Date(), '%Y-%m-%d %H:%M:%S', true)}`
  const project = 'PAN'
  const component = `iOS`
  const squad = `Others`

  const output= await ticketController.create(summary,project,component,squad)
  const data = JSON.parse(output)
  if (data) {
    console.log(
      `Ticket ${data.key} created successfully`
    )
  } else {
    console.log(
      `Ticket could not be created`
    )
  }

  const key = data.key

  let ticket = await ticketController.findByKey(key)
  if (ticket){
    const result = await ticket.erase()
    console.log(
      `Result from delete ${result}`
      )

  }else{
    console.log(
      `Cannot delete. Ticket does not exist`
    )
  }
}

async function showArrayOfIssues(array){
  const jql = atlassianClient.buildJQLQueryWithIssues(array)
  const data = await atlassianClient.getJiraJQL(jql)
  let issues = []
      
  for(const id in data["issues"])
  {
      // Check the data is valid and the Jira fields exist
      if(data["issues"][id] && data["issues"][id].fields) {  
        const type = data["issues"][id].fields.issuetype.name
        const key = data["issues"][id].key
        const reporter = data["issues"][id].fields.reporter.displayName
        const summary = data["issues"][id].fields.summary
        const status = data["issues"][id].fields.status.name
        issues.push([type,key, summary, reporter, status])// adding each issue data to 2D array
      }
  }
      
  console.log(
    `Result from Test ${issues}`
  )

}
async function showSubtasks(key){
  const ticketController = new TicketController()
  let ticket = await ticketController.findByKey(key)
  if (ticket){
    console.log(
      `Now fetching the subtasks...`
      )
    const issues = await ticket.getSubtasks()
    console.log(
      `subtasks: ${issues}`
    )
  }else{
    console.log(
      `Ticket does not exist`
      )
  }
}

async function setVersion(key,version){
  const ticketController = new TicketController()
  let ticket = await ticketController.findByKey(key)
  if (ticket){
    console.log(
      `Now setting version...`
      )
    const result = await ticket.setFixVersion(version)
    console.log(
      `Result: ${result}`)
  }else{
    console.log(
      `Ticket does not exist`
      )
  }
}

async function setStatus(key,next_transition,resolution){
  const ticketController = new TicketController()
  let ticket = await ticketController.findByKey(key)
  if (ticket){
    console.log(
      `Now setting status...`
      )
    const result = await ticket.setStatus(next_transition,resolution)
    console.log(
      `Result: ${result}`
      )
  }else{
    console.log(
      `Ticket does not exist`
      )
  }
}

async function transitionAllTheWay(key,status,resolution){
  const ticketController = new TicketController()
  const ticket = await ticketController.findByKey(key)
  if (ticket){
    const result = await ticket.transitionAllTheWayToStatus(status,resolution)
    console.log(
      `Result: ${result.message}`
      )
  }else{
    console.log(
      `Ticket does not exist`
      )
  }
}


```
This is how you should call each of the examples from an async function
```javascript
  console.log("Show ticket")
  await show('PAN-23076')

  console.log("Create and Erase ticket")
  await createAndErase()

  console.log("Get tickets from Array")
  await showArrayOfIssues(Array('PAN-23512','PAN-22158'))

  console.log("Show subtasks")
  await showSubtasks('PAN-22881')

  console.log("Set version")
  await setVersion('PAN-22158','iOS 11.4.0')

  console.log("Set status")
  await setStatus("PAN-23512",81,"Done")

  console.log("Transition all the way")
  await transitionAllTheWay("PAN-23512","Done","Done")

```

Manipulating Version - Example Usage
------------------------------------
```javascript
const VersionController = require('panamera-atlassian/VersionController')
const versionController = new VersionController()
const Version = require('panamera-atlassian/Version')

async function showById(id){
    let version = await versionController.findById(id)
    if (version){
        console.log(
            `Result from Test ${version.name}`
            )
    }else{
        console.log('Version does not exist')
    }
}

async function showByName(name){
    let version = await versionController.findByName(name)
    if (version){
        console.log(
            `Result from Test ${version.id}`
            )
    }else{
    console.log('Version cannot be found or has no tickets assigned. In that case you need the ID and call findById(id)')
    }
}

async function showAllVersions(project){
    let versions = await versionController.getAllProjectVersion(project)
    console.log(
        `Result from Test ${versions}`
    )
  
}

async function showTickets(name){
    let version = await versionController.findById(name)
    if (version){
        const issues = await version.getTickets()
        console.log(
            `Result from Test ${issues}`
            )
    }else{
        console.log(
            `Version cannot be found or has no tickets assigned. In that case you need the ID and call findById(id)`
            )
    }
  
}

async function showRN(name){
    let version = await versionController.findByName(name)
    if (version){
        const output = await version.getReleaseNotes("md")  //or empty for default
        console.log(
            `Result from Test: ${output}`
            )
    }else{
        console.log(
            `Version cannot be found or has no tickets assigned. In that case you need the ID and call findById(id)`
            )
    }
}

async function createVersion(){
    let version = new Version("Prueba")
    version.description = "A description"
    version.startDate = '2019-1-9'
    version.releaseDate = '2019-1-11'
    const output = await version.create()
    
    if (!output) {
        console.log('Could not create a JIRA Version')
        return false
      }
    const data = JSON.parse(output)
    return data.id
  
}

async function editVersion(id){
    let version = await versionController.findById(id)
    if (!version){
        console.log(`Version does not exist`)
        return false
    }
    version.description = "A new description"
    version.name = "New prueba"
    version.startDate = '2019-2-9'
    version.releaseDate = '2019-2-11'
    const output = await version.save()
    if (!output) {
        console.log('Could not edit a JIRA Version')
        return false
    }
    return true
}

async function releaseVersion(id){
    let version = await versionController.findById(id)
    if (!version){
        console.log(`Version does not exist`)
        return false
    }
    const output = await version.release()
    if (!output) {
        console.log('Could not release a JIRA Version')
        return false
    }
    return true
}


async function eraseVersion(id){
    let version = await versionController.findById(id)
    if (!version){
        console.log(`Version does not exist`)
        return false
    }
    const output = await version.erase()
    if (!output) {
        console.log('Could not delete a JIRA Version')
        return false
    }
    return true
}


```

This is how you should call each of the examples from an async function
```javascript

    console.log("Show by Id")
    await showById('30648')
  
    console.log("Show by name")
    await showByName('iOS 11.4.0')

    console.log("Show project versiones")
    await showAllVersions('PAN')

    console.log("Show tickets of a version")
    await showTickets('30648')
  
    console.log("Show RN of a version")
    await showRN('iOS 11.4.0')


    console.log("Create a version")
    const id = await createVersion()

    console.log("Edit a version")
    await editVersion(id)

    console.log("Release a version")
    await releaseVersion(id)

    console.log("Erase a version")
    await eraseVersion(id)

```

Manipulating Confluence Page - Example Usage
-----------------------------------
```javascript
const PageController = require('panamera-atlassian/PageController')

async function show(id){
  const pageController = new PageController()
  let page = await pageController.findById(id)
  if (page){
    console.log(
      `Found page and its title is: ${page.title}`
      )
  }else{
    console.log(
      `Page does not exist`
      )
  }
}

function update(id, content){
  const pageController = new PageController()
  try {
    pageController.findById(id)
      .then(page => {
        console.log(`Title: ${page.title}`)
        const currentDate = new Date()
        const localDateString = currentDate.toLocaleDateString({
          day: 'numeric',
          month: 'short',
          year: 'numeric'
        })
  
       const localTimeString = currentDate.toLocaleTimeString({
          hour: '2-digit',
          minute: '2-digit',
          second: '2-digit'
        })
  
        const dateString = `${localDateString} - ${localTimeString}`
        page.body = `${content} - ${dateString}`
        return page.updateBody()
      })
      .then(() => {
        console.log(`Succesfully updated page`)
      })
    
      .catch(e => console.error(`Error promise: (${e})`));
  } catch (e) {
    console.error(`Error outside: (${e})`);
  }
}

async function runAll(){
  console.log("Show page title")
  await show('818413576')

  console.log("Update page")
  const content = 'Hello world'
  await update('818413576', content)
}
  
runAll()
```
