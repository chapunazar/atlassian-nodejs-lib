/* eslint-disable no-console */
/**
 * API to conect to JIRA v3 rest api.
 * @module API
 * @author Manuel Nazar Anchorena
 */

const https = require('https')

/**
 * Returns basic encrypted credentials.
 * @returns {string} returns the user token encoded in base 64
 */

function getBasicCredentials() {
  if (process.env.ATLASSIAN_ACCESS_TOKEN) {
    const encCred = process.env.ATLASSIAN_ACCESS_TOKEN
    return encCred
  }
  return false
}

/**
 * Queries the JIRA API in GET method
 * @param {string} url - Url of resource to query against. Example: '/rest/api/3/version/12345'
 * @returns {Promise} returns a promise with the API result
 */
function getHttp(url) {
  const encCred = getBasicCredentials()
  if (!encCred) {
    return Promise.reject(new Error('API ERROR: Missing Token. Check env variable: ATLASSIAN_ACCESS_TOKEN.'))
  }

  const options = {
    hostname: 'naspersclassifieds.atlassian.net',
    path: url,
    port: 443,
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Accept-Charset': 'utf-8',
      'Content-Type': 'application/json',
      Authorization: `Basic ${encCred}`,
    },
  }
  return new Promise((resolve, reject) => {
    try {
      const req = https
        .request(options, res => {
          // console.log('Status Code: ', res.statusCode)
          // console.log('headers:', res.headers)
          let output = ''
          const rspns = res.statusCode
          res.on('data', data => {
            output += data
          })
          res.on('end', () => {
            if (rspns < 200 || rspns > 300) {
              reject({ status: rspns, data: output })
              return;
            }
            // Ended fine but if output is empty then at least return true
            if (output === '') {
              output = true
            }
            resolve(output)
          })
        })
        .on('error', e => reject(e))
      req.end()
    } catch (error) {
      reject(error)
    }
  })
}

/**
 * Queries the JIRA API in GET method
 * @param {string} method - Method can be "POST|PUT|DELETE|PATCH"
 * @param {string} url - Url of resource to query against. Example: 'version/'
 * @param {string} payload - JSON stringified that will be part of the http body
 * @returns {Promise} returns a promise with the API result
 */
function sendHttp(method, url, payload) {
  const encCred = getBasicCredentials()
  if (!encCred) {
    return false
  }
  const options = {
    hostname: 'naspersclassifieds.atlassian.net',
    path: url,
    port: 443,
    method,
    headers: {
      Accept: 'application/json',
      'Accept-Charset': 'utf-8',
      'Content-Type': 'application/json',
      Authorization: `Basic ${encCred}`,
    },
  }

  return new Promise((resolve, reject) => {
    const req = https
      .request(options, res => {
        // console.log('statusCode: ', res.statusCode);
        // console.log('headers:', res.headers);
        let output = ''
        const rspns = res.statusCode
        res.on('data', data => {
          output += data
        })
        res.on('end', () => {
          if (rspns < 200 || rspns > 300) {
            reject({ status: rspns, data: output })
            return;
          }
          // Ended fine but if output is empty then at least return true
          if (output === '') {
            output = true
          }
          resolve(output)
        })
      })
      .on('error', e => reject(e))

    req.write(payload)
    req.end()
  })
}

module.exports = {
  getBasicCredentials,
  getHttp,
  sendHttp
}
