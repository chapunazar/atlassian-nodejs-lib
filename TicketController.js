/**
 * Represents a Jira ticket controller.
 * @module TicketController
 * @author Manuel Nazar Anchorena
 */

/* eslint-disable camelcase */

const Ticket = require('./Ticket')
const AtlassianClient = require('./AtlassianClient')

const atlassianClient = new AtlassianClient()

module.exports = class TicketController {

  /**
   * Find a Ticket by Key.
   * @param {string} key - Jira ticket key example 'PAN-1234'.
   * @returns {object} if success its an instance of Ticket or false if unsuccessful
   */

  async findByKey(key) {
    const data = await this.getTicketData(key)
    if (data) {
      return new Ticket(key, data.fields)
    }
    return false

  }

  /**
   * Queries the API for the ticket data. Its a helper function for findbyKey. Should not be used.
   * @param {string} key - Jira ticket key example 'PAN-1234'.
   * @returns {JSON} The API response in JSON type
   */

  async getTicketData(key) {
    const url = `issue/${key}`
    return await atlassianClient.getFromJira(url)
  }

  /**
   * Creates a ticket in Jira.
   * @param {string} summary - Issue title.
   * @param {string} project - Jira project where the issue belongs
   * @param {string} component - component where the issue belong
   * @param {string} squad - Squad of the issue
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async create(summary, project, component, squad) {
    const url = 'issue/'
    // payload
    const data = {
      update: {},
      fields: {
        project: {
          key: project
        },
        summary,
        description: {
          type: 'doc',
          version: 1,
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'description'
                }]
            }]
        },
        issuetype: {
          name: 'Task'
        },
        components: [{
          name: component
        }],
        customfield_15925: {
          value: squad
        }
      }
    }

    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('POST', url, payload)
  }
}
