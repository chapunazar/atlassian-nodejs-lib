/**
 * Represents a Jira version controller.
 * @module TicketController
 * @author Manuel Nazar Anchorena
 */
const Version = require('./Version')
const AtlassianClient = require('./AtlassianClient')
const atlassianClient = new AtlassianClient()


module.exports = class VersionController {

  /**
   * Find a version by name. In order to work at least 1 ticket should have the version tagged in FIX Version field
   * @param {string} name - Version name. For example 'iOS 11.1.0'
   * @returns {object} if success its an instance of Version or false if unsuccessful
   */
  async findByName(name) {
    const data = await this.getVersionData(name)
    if (data) {
      const version = new Version(name, data.description, data.startDate, data.releaseDate)
      version.id = data.id
      version.fields = data
      return version
    }
    return false
  }

  /**
   * Find a version by id. As we have the id it doesnt need to run getVersionData
   * @param {string} id - Version id. For example '12345'
   * @returns {object} if success its an instance of Version or false if unsuccessful
   */
  async findById(id) {
    const url = `version/${id}`
    const data = await atlassianClient.getFromJira(url)
    if (data) {
      const version = new Version(data.name, data.description, data.startDate, data.releaseDate)
      version.id = data.id
      version.fields = data
      return version
    }
    return false
  }

  /**
   * Queries the API for the version data. It must always be called for filling in an instance of a Version.
   * @param {string} name - Jira Version name example 'iOS 11.1.0'.
   * @returns {JSON} if success its actually a string with the whole transition route or false if unsuccessful
   */

  async getVersionData(name) {
    // First we need to find the ID
    const url = `project/PAN/version?maxResults=1&orderBy=-sequence&query=${name}&startAt=0`
    const data = await atlassianClient.getFromJira(url)
    if (data.total > 0) {
      return data.values[0]
    }
  }

  /**
   * Gets all Versions that belong to a Jira project
   * @param {string} projectKey - Jira Project key example 'PAN'.
   * @returns {JSON} The API response in JSON type or false if empty
   */

  async getAllProjectVersion(projectKey) {
    const url = `project/${projectKey}/versions`
    const data = await atlassianClient.getFromJira(url)
    if (data) {
      return data
    }
    return false
  }
}
