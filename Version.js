/**
 * Represents a Jira Version.
 * @module Ticket
 * @author Manuel Nazar Anchorena
 */

const AtlassianClient = require('./AtlassianClient')
const atlassianClient = new AtlassianClient()

module.exports = class Version {

  /**
   * Creates an instance of object type Version
   * @param {string} name - Jira version name 'iOS 11.2.0'.
   */

  constructor(name, description, startDate, releaseDate) {
    this.name = name
    this.description = description
    this.startDate = startDate
    this.releaseDate = releaseDate
  }

  /**
   * Gets the Version's list of tickets that have the fix version corresponding to this version.
   * @returns {array} if success the list of tickets with some basic data. If there is no tickets then returns false.
   */

  async getTickets() {
    const jql = `project=PAN and fixVersion='${
      this.name
    }' order by type&fields=*all`
    const data = await atlassianClient.getJiraJQL(jql)

    const issues = data.issues
      .filter(issue => issue && issue.fields) //  Check the data is valid and the Jira fields exist
      .map(issue => {
        const key = issue.key
        const summary = issue.fields.summary
        const type = issue.fields.issuetype.name
        const status = issue.fields.status.name
        let squad = ''
        let assignee = ''
        let reporter = ''
        let fixversion = ''
        let resolution = ''

        try {
          squad = issue.fields.customfield_15925.value
        } catch (e) {
          squad = '<none>'
        }

        try {
          fixversion = issue.fields.fixVersions[0].name
        } catch (e) {
          fixversion = '<none>'
        }

        try {
          assignee = issue.fields.assignee.name
        } catch (e) {
          assignee = '<none>'
        }

        try {
          resolution = issue.fields.resolution.name
        } catch (e) {
          resolution = '<none>'
        }

        try {
          reporter = issue.fields.reporter.displayName
        } catch (e) {
          reporter = '<none>'
        }

        return {
          key,
          summary,
          type,
          fixversion,
          status,
          resolution,
          squad,
          assignee,
          reporter,
        } //  adding each issue data to 2D array
      })

    return issues
  }

  /**
   * Returns a string with the list of tickets grouped by Squad, valid only for PAN project.
   * @param {string} format - Defines how the response will be formatted. If 'html' then it adds some html tags.
   * @returns {string} Text with the release notes
   */

  async getReleaseNotes(format) {
    const tickets = await this.getTickets()
    let outputTxt = ''

    // sort by Squad ascending and y key ascending
    // key, summary, type, fixversion, status, resolution, squad, assignee, reporter
    // tickets.sort([{column: 4, ascending: true}, {column: 0, ascending: true}]);

    const sortBy = (field, reverse, primer) => {
      const key = primer ? x => primer(x[field]) : x => x[field]
      const reverseInt = !reverse ? 1 : -1
      return (a, b) => {
        const newA = key(a)
        const newB = key(b)
        return reverseInt * ((newA > newB) - (newB > newA))
      }
    }

    if (tickets.length === 0) {
      return ''
    }

    tickets.sort(sortBy('squad', false, String))

    let prevSquad = ''
    if (format === 'html') {
      outputTxt += '<p>'
    }

    tickets.forEach(ticket => {
      const number = ticket.key
      const summary = ticket.summary
      // const type = ticket.type // not used now, for future use
      const newSquad = ticket.squad

      if (prevSquad !== newSquad) {
        if (format === 'html') {
          outputTxt += `</p><p><h3>${newSquad}:</h3>`
        } else if (format === 'md') {
          outputTxt += `\n${newSquad}:\n`
        } else {
          outputTxt += `\n${newSquad}:\n`
        }
      }

      if (format === 'html') {
        outputTxt += `[<a href="https://naspersclassifieds.atlassian.net/browse/${number}">${number}</a>]` +
        `- ${summary}<br />`
      } else if (format === 'md') {
        outputTxt += `[<https://naspersclassifieds.atlassian.net/browse/${number}|${number}>] - ${summary}\n`
      } else {
        outputTxt += `[${number}] - ${summary}\n`
      }

      //  the new squad will be the prevSquad in next iteration
      prevSquad = newSquad
    })

    if (format === 'html') {
      outputTxt += '</p>'
    }

    return outputTxt
  }

  /**
   * Creates a version in Jira. Should be used only when an instance is initialized
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async create() {
    const url = 'version/'
    // payload
    const data = {
      description: this.description,
      name: this.name,
      startDate: this.startDate,
      releaseDate: this.releaseDate,
      project: 'PAN', // hardcoded for security
    }

    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('POST', url, payload)
  }

  /**
   * Saves an existing version in Jira.
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async save() {
    const url = `version/${this.id}`
    // payload
    const data = {
      id: this.id,
      name: this.name,
      description: this.description,
      startDate: this.startDate,
      releaseDate: this.releaseDate,
    }

    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('PUT', url, payload)
  }

  /**
   * Releases a version in Jira.
   * @param {string} date - Release Date. If date is not specified will check if it already has one else use Now()
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async release(date) {
    if (!date) {
      if (typeof this.releaseDate === 'undefined' || this.releaseDate === '' || this.releaseDate === null) {
        // Empty -> use now
        this.releaseDate = atlassianClient.dateFormat(new Date(), '%Y-%m-%d', true)
      }
    } else {
      // override with the one in parameter
      this.releaseDate = date
    }

    const url = `version/${this.id}`
    // payload
    const data = {
      id: this.id,
      released: true,
      releaseDate: this.releaseDate,
    }

    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('PUT', url, payload)
  }

  /**
   * Delete an existing version in Jira.
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async erase() {
    const url = `version/${this.id}/removeAndSwap`
    // payload
    const data = {}
    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('POST', url, payload)
  }
}
