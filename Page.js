/**
 * Represents a Confluence Page.
 * @module Page
 * @author Manuel Nazar Anchorena
 */

const AtlassianClient = require('./AtlassianClient')
const atlassianClient = new AtlassianClient()

module.exports = class Page {

  /**
   * Create a Ticket instance.
   * @param {string} id - Page id. For example '12345'
   */
  constructor(id) {
    this.id = id
    this.title = null
    this.body = null
    this.version = null
    this.author = null
    this.data = null
  }

  /**
   * Updates the Page body
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async updateBody() {
    const newVersion = this.version + 1
    const url = this.id
    // payload
    const data = {
      id: this.id,
      type: 'page',
      title: this.title,
      space: { key: 'GH' },
      body: { storage: { value: this.body, representation: 'storage' } },
      version: { number: newVersion }
    }

    const payload = JSON.stringify(data)
    return await atlassianClient.sendToConfluence('PUT', url, payload)
  }
}
