/**
 * Represents a Confluenc Page controller.
 * @module PageController
 * @author Manuel Nazar Anchorena
 */
const AtlassianClient = require('./AtlassianClient')
const atlassianClient = new AtlassianClient()
const Page = require('./Page')

module.exports = class PageController {

  /**
   * Find a Page by id.
   * @param {string} id - Page id. For example '12345'
   * @returns {object} if success its an instance of Page or false if unsuccessful
   */
  async findById(id) {
    const page = new Page(id)
    const data = await this.getPageData(id)
    if (data) {
      page.id = data.id
      page.title = data.title
      page.body = data.body.storage.value
      page.version = data.version.number
      page.author = data.history.createdBy.username
      page.fields = data
      return page
    }
    return false
  }

  async getPageData(id) {
    // Now we need to get the Version details
    const url = `${id}?expand=body.storage,version,history`
    return await atlassianClient.getFromConfluence(url)
  }

}
