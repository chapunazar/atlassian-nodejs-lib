/**
 * Represents a Jira ticket.
 * @module Ticket
 * @author Manuel Nazar Anchorena
 */

const AtlassianClient = require('./AtlassianClient')
const atlassianClient = new AtlassianClient()

const BugWF = [
  {
    id: 681,
    name: 'ANALYSIS'
  },
  {
    id: 611,
    name: 'READY TO PRIORITIZE'
  },
  {
    id: 621,
    name: 'READY TO DEV'
  },
  {
    id: 151,
    name: 'IN PROGRESS'
  },
  {
    id: 201,
    name: 'IN CODE REVIEW'
  },
  {
    id: 251,
    name: 'READY FOR TESTING'
  },
  {
    id: 581,
    name: 'IN TEST'
  },
  {
    id: 591,
    name: 'TEST DONE'
  },
  {
    id: 631,
    name: 'IN STAGING'
  },
  {
    id: 601,
    name: 'DONE'
  }
]

const ElseWF = [
  {
    id: 81,
    name: 'DONE'
  }
]

// These statuses are final. Means if a ticket is in one of these it wont move anywhere
const ExcludeStatus = ['DONE', 'CLOSED', 'REJECTED']


module.exports = class Ticket {

  /**
   * Create a Ticket instance.
   * @param {string} key - The key value.
   * @param {string} fields - Field of the ticket.
   */
  constructor(key, fields) {
    this.key = key
    this.fields = { ...fields }
  }


  /**
   * Delete an existing ticket in Jira.
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async erase() {
    const url = `issue/${this.key}/`
    // payload
    const data = {}
    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('DELETE', url, payload)
  }

  /**
   * Get all subtasks of a specific ticket.
   * @returns {array} with basic data: priority, key, summary, issuetype, status
   */
  getSubtasks() {
    const subtasks = this.fields.subtasks || []
    return subtasks.map(_ => new Ticket(_.key, _.fields))
  }

  /**
   * Sets a fix version.
   * @param {string} fixversion - Fix version name
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */
  async setFixVersion(fixversion) {
    const url = `issue/${this.key}`

    // this will be the payload
    const data = {
      update: {
        fixVersions: [{ set: [{ name: fixversion }] }],
      },
    }
    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('PUT', url, payload)
  }

  /**
   * Get All transitions of a Ticket
   * @returns {Array<Transition>} if success its return transition information.
   */
  async getTransitions() {
    const url = `issue/${this.key}/transitions`
    const response = await atlassianClient.getFromJira(url)
    return response.transitions
  }

  /**
   * Get a specific transition of a Ticket
   * @param {string} transitionName - Must be a valid resolution value. Check workflow
   * @returns {Transition} if success its return transition information.
   */
  async getTransition(transitionName) {
    return (await this.getTransitions()).find(_ => _.name.toUpperCase() === transitionName.toUpperCase())
  }

  /**
   * Transitions Ticket to a specified transition id.
   * @param {number} nextTransitionId - Must be a valid next transition. Check workflow.
   * @param {string} resolution - Optional - Must be a valid resolution value. Check workflow
   * @returns {boolean} if success its actually a string with the API response or false if unsuccessful
   */

  async setStatus(nextTransitionId, resolution) {
    const url = `issue/${this.key}/transitions`
    const data = {
      transition: {
        id: nextTransitionId,
      }
    }
    if (resolution) {
      data.fields = {
        resolution: {
          name: resolution
        }
      }
    }
    const payload = JSON.stringify(data)
    return await atlassianClient.sendToJira('POST', url, payload)
  }

  /**
   * Gets the specific workflow for the issue type if the instance (ticket)
   * Options are 2: bug or (task,subtask,defect) so far
   * @returns {Array}
   */

  getWorkflow() {
    // Depending on type we use one or the other workflow
    switch (this.fields.issuetype.name) {
      case 'Bug':
        return BugWF
      default:
        return ElseWF
    }
  }

  /**
   * Transition a ticket to 'Done'. Only valid for PAN project.
   * @returns {{success: boolean, message: string}}
   * Success is a boolean with value true if the issue was transitioned.
   * Message is actually a string with the whole transition route or false if unsuccessful
   */
  async transitionToDone() {
    return this.transitionAllTheWayToStatus('Done', 'Done')
  }

  /**
   * Transition a ticket to a destination status no matter where it is. Only valid for PAN project.
   * @param {string} finalStatus - Must be a valid final transition. Check workflow.
   * @param {string} resolution - Optional - Must be a valid resolution value. Check workflow.
   * @returns {{success: boolean, message: string}}
   * Success is a boolean with value true if the issue was transitioned.
   * Message is actually a string with the whole transition route or false if unsuccessful
   */
  async transitionAllTheWayToStatus(finalStatus, resolution) {

    const WF = this.getWorkflow()

    const finalStatusUP = finalStatus.toUpperCase()

    // Check destination status is a valid status
    if (!WF.find(_ => _.name === finalStatusUP)) {
      return Promise.reject('Failed, status not valid.')
    }


    let currentStatus = this.fields.status.name.toUpperCase()
    if (currentStatus === finalStatus.toUpperCase()) {
      return Promise.resolve({
        success: true
      })
    }

    //  Check current status is not in the excluded list
    if (ExcludeStatus.find(_ => _ === currentStatus)) {
      return Promise.reject('Current status in excluded list.')
    }

    let resultTxt = `|${currentStatus}.`
    let success = false

    // Find position in vector. if result is -1 the next one will be 0 anyway
    let position = WF.findIndex(_ => _.name === currentStatus)

    // Iterate till desired transition is reached
    while (currentStatus !== finalStatusUP) {
      // Find next transition
      position += 1
      const { id: nextTransitionId, name: nextStatus } = WF[position]

      const result = await this.setStatus(nextTransitionId, (position === (WF.length - 1)) && resolution)
      if (result) {
        resultTxt += `${nextStatus} Ok.`
        currentStatus = nextStatus
        // success will be overwritten on every successful transition but its ok. That last one is which matters.
        success = true
      } else {
        return Promise.reject(`Failed to transition to ${nextStatus}: ${result}.`)
      }
    }

    resultTxt += 'Set Status finish.|'

    return {
      success,
      message: resultTxt
    }
  }
}
