/**
 * Represents the Atlassian entity for general settings and function.
 * @module AtlassianClient
 * @author Manuel Nazar Anchorena
 */
const jiraApiVersion = '/rest/api/3/' // this will be prepended to the resource url

const confluenceApiVersion = '/wiki/rest/api/content/' // this will be prepended to the resource url
const API = require('./API')

module.exports = class AtlassianClient {

  /**
   * Returns the full URL to send to API.getHttp.
   * @param {string} url - Resourse URL ie: 'issue/PAN-12345'
   * @returns {string} Full url without hostname ie: /rest/api/3/issue/PAN-12345
   */

  prepareJiraURL(url) {
    return `${jiraApiVersion}${encodeURI(url)}`
  }

  /**
   * Returns the full URL to send to API.getHttp.
   * @param {string} url - Resourse URL ie: 'content/12345'
   * @returns {string} Full url without hostname ie: /wiki/rest/api/content/12345
   */

  prepareConfluenceURL(url) {
    return `${confluenceApiVersion}${encodeURI(url)}`
  }

  /**
   * Queries the API to get Jira data
   * @param {string} url - Url of resource to query against. Example: '/rest/api/3/version/12345'
   * @returns {Promise} returns a promise with the API result
   */
  async getFromJira(url) {
    const data = await API.getHttp(this.prepareJiraURL(url))
    return JSON.parse(data)
  }

  /**
   * Sends to the Jira API data
   * @param {string} method - Method can be "POST|PUT|DELETE|PATCH"
   * @param {string} url - Url of resource to query against. Example: 'version/'
   * @param {string} payload - JSON stringified that will be part of the http body
   * @returns {Promise} returns a promise with the API result
   */
  async sendToJira(method, url, payload) {
    return await API.sendHttp(method, this.prepareJiraURL(url), payload)
  }

  /**
   * Queries the API to get Confluence data
   * @param {string} url - Url of resource to query against. Example: 'content/12345'
   * @returns {Promise} returns a promise with the API result
   */
  async getFromConfluence(url) {
    const data = await API.getHttp(this.prepareConfluenceURL(url))
    return JSON.parse(data)
  }

  /**
   * Sends to the Confluence API data
   * @param {string} method - Method can be "POST|PUT|DELETE|PATCH"
   * @param {string} url - Url of resource to query against. Example: 'version/'
   * @param {string} payload - JSON stringified that will be part of the http body
   * @returns {Promise} returns a promise with the API result
   */
  async sendToConfluence(method, url, payload) {
    return await API.sendHttp(method, this.prepareConfluenceURL(url), payload)
  }

  /**
   * Queries the JIRA api qith a JQL query
   * @param {string} jql - Jira Query Language.
   * @returns {JSON} returns a JSON with the list rows as result of the JQL or false if empty
   */
  async getJiraJQL(jql) {
    const url = 'search'
    let done = false
    let startAt = 0
    const maxResults = 50 // Pagination API
    let issues = []
    let data = ''

    while (!done) {
      const queryURL = `${url}?jql=${jql}&startAt=${startAt}&maxResults=${maxResults}`
      const out = await API.getHttp(this.prepareJiraURL(queryURL))
      data = JSON.parse(out)
      issues = issues.concat(data.issues)
      if (startAt + maxResults >= data.total) {
        done = true
        // console.log(`End of pagination " + ${(startAt+maxResults)}`)
      } else {
        startAt += maxResults
      }
    } //  while not done

    const output = {
      total: data.total,
      issues
    }

    if (data.total > 0) {
      return output
    }
    return false
  }


  /**
  * Transforms an array of issues into a JQL format to query them once instead of 1 by one
  * @param {array} issues - Array with a list of issues.
  * @returns {string} JQL with the list of issues.
  * @example
  * const jql = atlassianClient.buildJQLQueryWithIssues(['PAN-1234','PAN-1235','PAN-1236'])
  */

  buildJQLQueryWithIssues(issues) {
    return `issueKey in (${issues.join(', ')})`
  }


  /**
  * Formats a Date object
  * @param {Date} date - Date instance to format
  * @param {string} fstr - Format type. Example '%Y-%m-%d %H:%M:%S'
  * @param {boolean} utc - Specifies if the Date is in UTC time
  * @returns {string} Formatted date
  * @example
  * const dateStr = atlassianClient.dateFormat(new Date (), '%Y-%m-%d %H:%M:%S', true)
  */

  dateFormat(date, fstr, utc) {
    const utcStr = utc ? 'getUTC' : 'get'
    return fstr.replace(/%[YmdHMS]/g, m => {
      let mStr = ''
      switch (m) {
        case '%Y':
          return date[`${utcStr}FullYear`]()// no leading zeros required
        case '%m':
          mStr = 1 + date[`${utcStr}Month`]()
          break
        case '%d':
          mStr = date[`${utcStr}Date`]()
          break
        case '%H':
          mStr = date[`${utcStr}Hours`]()
          break
        case '%M':
          mStr = date[`${utcStr}Minutes`]()
          break
        case '%S':
          mStr = date[`${utcStr}Seconds`]()
          break
        default:
          return mStr.slice(1) // unknown code, remove %
      }
      // add leading zero if required
      return (`0${mStr}`).slice(-2)
    })
  }
}
